﻿using RPG_Heroes.ItemExceptions;
using RPG_Heroes.Items;
using RPG_Heroes.Subclasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using static RPG_Heroes.Items.Item;

namespace RPG_Heroes
{
    public abstract class Hero
    {
        // Properties
        public string Name { get; set; }
        public int Level { get; set; }
        public HeroAttribute LevelAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public List<WeaponType> ValidWeaponTypes { get; set; }
        public List<ArmorType> ValidArmorTypes { get; set; }

        // Default constructor
        protected Hero()
        {
        }

        protected Hero(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();
        }

     
        // Creating an overloading constructor using all parameters
        public Hero(string name, int level, HeroAttribute levelAttributes, Dictionary<Slot, Item> equipment,
            List<WeaponType> validWeaponTypes, List<ArmorType> validArmorTypes)
        {
            Name = name;
            Level = 1;
            LevelAttributes = levelAttributes;
            Equipment = new Dictionary<Slot, Item>();
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();
        }



        /// <summary>
        /// Abstract method for levelling up
        /// </summary>
        /// <param name="heroAttribute">name of the various attributes contained in the HeroAttribute class.</param>
        /// <param name="level">name of the level for a hero.</param>
        public abstract void LevelUp(HeroAttribute heroAttribute, int level);


        // Method for equiping a weapon.
        public void EquipWeapon(Weapon item)
        {
            if (Level >= item.RequiredLevel && ValidWeaponTypes.Contains(item.WeaponType)) // Conditions that needs to be fulfilled
            {
                Equipment[item.Slot] = item;    
            }
            throw new InvalidWeaponException("Invalid weapon name resulted in an error");
        }



        // Method to equip some armor if both conditions are fulfilled
        public void EquipArmor(Armor item)
        {
            if (Level >= item.RequiredLevel && ValidArmorTypes.Contains(item.ArmorType))
            {
                Equipment[item.Slot] = item;
            }
            throw new InvalidArmorException("Invalid armor name resulted in an error");
        }


        // Calculation of the total hero attribute values
        public HeroAttribute TotalAttributes()
        {
            HeroAttribute total = LevelAttributes;

            foreach (var item in Equipment)        // Iterating over every key/value pair in the Equipment property
            {
                if (item.Key != Slot.Weapon)      // If the item slot is not a weapon slot
                {
                    Armor armor = (Armor)item.Value;  // Explicit Type conversion (Casting)
                    HeroAttribute.Add(armor.ArmorAttribute, total);
                }
            }   return total;
             
        }
        
        // Method for calculating damage
        public int Damage() 
        {
            foreach (var item in Equipment)
            {
                if (item.Key == Slot.Weapon && item.Value != null)   // Checking if a weapon is equipped
                {
                    Weapon equipWeapon = (Weapon)item.Value;
                    return equipWeapon.WeaponDamage * (1 + TotalAttributes().Strength / 100);   // Calculation of damage with a weapon equipped
                }
            }
            // Calculation of damage without a weapon equipped
            return 1 * (1 + TotalAttributes().Strength / 100);
        }

        public void Display()
        {
            StringBuilder sb = new StringBuilder($"Displaying hero status: ");
            sb.AppendLine($"Character name is{Name}");
            sb.AppendLine($"{Level}");
            sb.AppendLine($"{TotalAttributes().Strength}");
            sb.AppendLine($"{TotalAttributes().Dexterity}");
            sb.AppendLine($"{TotalAttributes().Intelligence}");

            Console.WriteLine(sb.ToString());
        }
    }
}
