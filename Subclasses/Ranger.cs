﻿using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Subclasses
{
    public class Ranger : Hero
    {

        // Default constructor
        public Ranger() : base() { }



        // Overloaded constructor which takes in name as a parameter
        public Ranger(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(1, 7, 1);
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Bows };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Leather, ArmorType.Mail };
        }

        // Polymorphism, each subclass gain levels differently
        public override void LevelUp(HeroAttribute levelAttributes, int level)
        {
            LevelAttributes.Strength += 1;
            LevelAttributes.Dexterity += 5;
            LevelAttributes.Intelligence += 1;
            Level++;
        }
    }
}
