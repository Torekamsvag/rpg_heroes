﻿using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Subclasses
{
    public class Rogue : Hero
    {
        public Rogue() : base() { }
        

        public Rogue(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(2, 6, 1);                                          
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Daggers, WeaponType.Swords };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Leather, ArmorType.Mail };
        }

        // Polymorphism, each subclass gain levels differently
        public override void LevelUp(HeroAttribute levelAttributes, int level)
        {
            LevelAttributes.Strength += 1;
            LevelAttributes.Dexterity += 4;
            LevelAttributes.Intelligence += 1;
            Level++;
        }

    }
}
