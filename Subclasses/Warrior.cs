﻿using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Subclasses
{
    public class Warrior : Hero
    {
        public Warrior() : base() { }

        // Overloaded constructor
        public Warrior(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(5, 2, 1);
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Axes, WeaponType.Hammers, WeaponType.Swords };
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Mail, ArmorType.Plate };
        }
        

        // Polymorphism, each subclass gain levels differently
        public override void LevelUp(HeroAttribute levelAttributes, int level)
        {
            LevelAttributes.Strength += 3;      // Increasing the attributes upon level up
            LevelAttributes.Dexterity += 2;
            LevelAttributes.Intelligence += 1;
            Level++;
        }
    }
}
