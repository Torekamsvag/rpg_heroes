﻿using RPG_Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Subclasses
{
    public class Mage : Hero     // Inheriting some features from the Hero class
    {
        // Default Constructor
        public Mage() : base() { }



        // Overloaded constructor
        // name parameter is inherited from the parent class (Hero)
        public Mage(string name) : base(name)
        {
            Level = 1;
            LevelAttributes = new HeroAttribute(1, 1, 8);                                                
            ValidWeaponTypes = new List<WeaponType>() { WeaponType.Staffs, WeaponType.Wands };    // Specifying which WeaponTypes are available
            ValidArmorTypes = new List<ArmorType>() { ArmorType.Cloth };                        // specifying which ArmorTypes are available
        }

 

        // Polymorphism, each subclass gain levels differently
        public override void LevelUp(HeroAttribute levelAttributes, int level)
        {
            LevelAttributes.Strength += 1;                   // Specifying the gain in the strength attribute upon level up 
            LevelAttributes.Dexterity += 1;                  // Specifying the gain in the dexterity attribute upon level up
            LevelAttributes.Intelligence += 5;               // Specifying the gain in the intelligence attribute upon level up
            Level++;                                         // Hero level increases by one upon level up
        }

        /*
        public override void EquipWeapon(Dictionary<Slot, Item> equipment)
        {
            equipment.Add(Slot.Weapon, );
        }
        */
        /*
        public override int Damage(Weapon item, HeroAttribute heroAttribute)
        {
            int heroDamage = item.WeaponDamage * (1 + LevelAttributes.Intelligence / 100);
            return heroDamage;
        */

    }
}
