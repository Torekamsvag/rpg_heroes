﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    public class Armor : Item  // Inheriting some features from the Item class
    {
        // Properties
        public HeroAttribute ArmorAttribute { get; set; }

        public ArmorType ArmorType { get; set; }

        // Overloading constructor
        public Armor(string name, int requiredLevel, Slot slot) : base(name, requiredLevel, slot)
        {
            ArmorAttribute = new HeroAttribute();
        }

        // Default constructor
        public Armor()
        {
        }

    }

    // Enum declaration for the armor type
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
