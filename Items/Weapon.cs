﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    public class Weapon : Item    // Inheriting some features from the Item class
    {
        // Properties
        public int WeaponDamage { get; set; }
        public WeaponType WeaponType { get; set; }

        // Constructor overloading
        public Weapon(string name, int requiredLevel, Slot slot, int weaponDamage) : base(name, requiredLevel, slot)
        {
            WeaponDamage = weaponDamage;
            WeaponType = new WeaponType();
        }

        // Default constructor
        public Weapon() { }
    }
    
    // Enum declaration for the weapon type
    public enum WeaponType
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands
    }
}
