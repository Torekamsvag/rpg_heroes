﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Items
{
    public abstract class Item : HeroAttribute
    {
        // Properties
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot { get; set; }

        // Creating a constructor with three parameters
        public Item(string name, int requiredLevel, Slot slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }

        // Default constructor
        protected Item()
        {
        }
    }

    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
