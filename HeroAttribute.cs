﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class HeroAttribute
    {
        // Properties
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        // Overloaded constructor containing three parameters
        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public HeroAttribute()
        {
        }

        // Static method for adding two instances together
        public static HeroAttribute Add(HeroAttribute h1, HeroAttribute h2)
        {
            return new HeroAttribute(h1.Strength + h2.Strength, h1.Dexterity + h2.Dexterity,
                h1.Intelligence + h2.Intelligence);
        }
    }
}
